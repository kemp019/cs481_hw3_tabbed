﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3tab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TheProtagonist : ContentPage
    {
        public TheProtagonist()
        {
            InitializeComponent();
        }

        //returns screen elements to the default color. This is a restorative power of the Protagonist made manifest and maybe foreshadowing.
        protected override void OnAppearing()
        {
            ((TabbedPage)Application.Current.MainPage).BarBackgroundColor = Color.Default;
            ((TabbedPage)Application.Current.MainPage).BarTextColor = Color.Default;
        }
        //an echo of knowledge once known
        protected override void OnDisappearing()
        {
            DisplayAlert("Knowledge Gained!", "After reading about the Protagonist you feel a vague sense of familiarity, as if you somehow now know yourself a bit better.", "Huh. That's odd.");
        }
        //on appear and on disappear. +10 points
    }
}