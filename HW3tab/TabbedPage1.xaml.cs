﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3tab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPage1 : TabbedPage
    {
        public TabbedPage1()
        {
            InitializeComponent();
            Children.Add(new TheProtagonist());
            Children.Add(new TheSnake());
            Children.Add(new TheSpider());
           // Children.Add(new TheEnigma()); //comment out if the enigma add idea works
        }
    }
}