﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3tab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TheSpider : ContentPage
    {
        bool insight;
        public TheSpider()
        {
            InitializeComponent();
            insight = false;
        }
        //does the spider classic and turns the title text black, to signify the fine fellow's presence.
        protected override void OnAppearing()
        {
            ((TabbedPage)Application.Current.MainPage).BarTextColor = Color.Black;
        }
        //insight is knowing you can learn a thing...
        protected override void OnDisappearing()
        {
            if (!insight)
            {
                DisplayAlert("A Secret Unveiled!", "Your review of the Spider has caused his insight to rub off on you, and new page is visible.", "The Enigma?");
                ((TabbedPage)Application.Current.MainPage).Children.Add(new TheEnigma());
                insight = true;
            }
        }
        //on appear and on disappear. +10 points
    }
}