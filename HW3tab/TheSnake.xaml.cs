﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3tab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TheSnake : ContentPage
    {
        public TheSnake()
        {
            InitializeComponent();
        }
        //snake bar!
        protected override void OnAppearing()
        {
            ((TabbedPage)Application.Current.MainPage).BarBackgroundColor = Color.ForestGreen;
        }
        //coiled around the background
        protected override void OnDisappearing()
        {
            ((TabbedPage)Application.Current.MainPage).BackgroundColor = Color.LightGreen;
        }
        //on appear and on disappear. +10 points
    }
}