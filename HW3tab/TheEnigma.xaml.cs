﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3tab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TheEnigma : ContentPage
    {
        public TheEnigma()
        {
            InitializeComponent();
        }

        //...wisdom is knowing you shouldn't
        protected override void OnAppearing()
        {
            DisplayAlert("A Sense of Unease...", "As you prepare to read the Enigma's page you feel a slight tightness in your gut. Perhaps this information had been better off hidden.", "Strange...");
            ((TabbedPage)Application.Current.MainPage).BarBackgroundColor = Color.Black;
            ((TabbedPage)Application.Current.MainPage).BarTextColor = Color.Red;
            ((TabbedPage)Application.Current.MainPage).BackgroundColor = Color.Black; 
        }
        //spoooky
        protected override void OnDisappearing()
        {
            DisplayAlert("A Finger Placed", "You finally find the words to describe the sense of unease that filled you upon reading the Enigma's page. Like staring into a mirror, but finding that the eyes staring back are not your own.", "...whoopsies");
            ((TabbedPage)Application.Current.MainPage).BackgroundColor = Color.Default;
            //((TabbedPage)Application.Current.MainPage).Children.Remove(TheEnigma); //removes the enigma page after reading for extra creepiness 
        }
        //on appear and on disappear. +10 points
    }
}